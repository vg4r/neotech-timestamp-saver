package net.sf.vgrs.neotech.tstmp.dao;

import com.mysql.cj.jdbc.MysqlDataSource;
import net.sf.vgrs.neotech.tstmp.util.AppConfig;

import javax.sql.DataSource;

import static net.sf.vgrs.neotech.tstmp.util.AppConfig.PropertyKeys.APP_DB_JDBC;
import static net.sf.vgrs.neotech.tstmp.util.AppConfig.PropertyKeys.APP_DB_JDBC_PASSWORD;
import static net.sf.vgrs.neotech.tstmp.util.AppConfig.PropertyKeys.APP_DB_JDBC_USER;

public class DataSourceFactory {

    public static DataSource getDataSource(){
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(AppConfig.getProperty(APP_DB_JDBC));
        dataSource.setUser(AppConfig.getProperty(APP_DB_JDBC_USER));
        dataSource.setPassword(AppConfig.getProperty(APP_DB_JDBC_PASSWORD));
        return dataSource;
    }

}
