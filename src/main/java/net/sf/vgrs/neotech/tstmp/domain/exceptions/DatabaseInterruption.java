package net.sf.vgrs.neotech.tstmp.domain.exceptions;

public class DatabaseInterruption extends UserDefinedException {
    public DatabaseInterruption() {
    }

    public DatabaseInterruption(String message) {
        super(message);
    }

    public DatabaseInterruption(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseInterruption(Throwable cause) {
        super(cause);
    }

    public DatabaseInterruption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
