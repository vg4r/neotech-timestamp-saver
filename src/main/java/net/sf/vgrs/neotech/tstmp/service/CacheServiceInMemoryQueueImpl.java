package net.sf.vgrs.neotech.tstmp.service;

import java.util.concurrent.BlockingQueue;

public class CacheServiceInMemoryQueueImpl<T> implements CacheService<T>{

    private BlockingQueue<T> cache;


    public CacheServiceInMemoryQueueImpl(BlockingQueue<T> cache){
        this.cache = cache;
    }

    @Override
    public void put(T element) throws InterruptedException {
        cache.put(element);
    }

    @Override
    public T get() throws InterruptedException {
        return cache.take();
    }
}
