package net.sf.vgrs.neotech.tstmp;

import net.sf.vgrs.neotech.tstmp.dao.TimestampDao;
import net.sf.vgrs.neotech.tstmp.domain.exceptions.DatabaseInterruption;
import net.sf.vgrs.neotech.tstmp.domain.exceptions.UserDefinedException;
import net.sf.vgrs.neotech.tstmp.service.CacheService;
import net.sf.vgrs.neotech.tstmp.util.AppConfig;
import java.util.Date;
import static net.sf.vgrs.neotech.tstmp.util.AppConfig.PropertyKeys.APP_DB_INTERRUPTION_MESSAGE;
import static net.sf.vgrs.neotech.tstmp.util.AppConfig.PropertyKeys.APP_DB_INTERRUPTION_TIMEOUT;

public class TimeStampConsumer implements Runnable{

    private CacheService<Date> cacheService;
    private TimestampDao timestampDao;

    public TimeStampConsumer(CacheService<Date> cacheService, TimestampDao timestampDao){
        this.cacheService = cacheService;
        this.timestampDao = timestampDao;
    }

    @Override
    public void run() {
        Date value = null;
        while (true){
            try{
                if (value == null){
                    value = cacheService.get();
                }
                timestampDao.create(value);
                value = null;
            } catch (DatabaseInterruption i){
                Long timeout = Long.valueOf(AppConfig.getProperty(APP_DB_INTERRUPTION_TIMEOUT));
                System.out.println(String.format(AppConfig.getProperty(APP_DB_INTERRUPTION_MESSAGE), timeout));
                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (UserDefinedException e){
                System.out.println("Some database exception occurred... trying immediately");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
