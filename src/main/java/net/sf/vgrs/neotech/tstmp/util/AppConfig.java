package net.sf.vgrs.neotech.tstmp.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AppConfig {
    private static Map<String, String> properties;

    private AppConfig() {

    }

    public static void init() throws IOException {
        init("application.properties");
    }

    public static void init(String filePath) throws IOException {
        if (properties == null) {
            Properties prop = new Properties();
            prop.load(new FileInputStream(filePath));
            init(prop);
        }
    }

    public static void init(Properties prop){
        if (properties == null) {
            properties = new HashMap<>();
            prop.forEach((key, value) -> properties.put(key.toString(), value.toString()));
            prop = null;
        }
    }

    public static void destroy(){
        properties = null;
    }

    public static enum PropertyKeys {
        APP_DB_PROVIDER("app.db.provider"),
        APP_DB_INTERRUPTION_TIMEOUT("app.db.interruption.timeout"),
        APP_DB_INTERRUPTION_MESSAGE("app.db.interruption.message"),
        APP_CACHE_PROVIDER("app.cache.provider"),
        APP_DB_JDBC("app.db.jdbc"),
        APP_DB_JDBC_USER("app.db.jdbc.user"),
        APP_DB_JDBC_PASSWORD("app.db.jdbc.password");

        PropertyKeys(String value) {
            this.value = value;
        }

        private String value;

        public String value() {
            return this.value;
        }
    }

    public static String getProperty(PropertyKeys propertyKey) {
        return properties.get(propertyKey.value());
    }

}
