package net.sf.vgrs.neotech.tstmp;

import net.sf.vgrs.neotech.tstmp.dao.DataSourceFactory;
import net.sf.vgrs.neotech.tstmp.dao.TimestampDao;
import net.sf.vgrs.neotech.tstmp.dao.TimestampDaoMySqlImpl;
import net.sf.vgrs.neotech.tstmp.service.CacheService;
import net.sf.vgrs.neotech.tstmp.service.CacheServiceInMemoryQueueImpl;
import net.sf.vgrs.neotech.tstmp.util.AppConfig;
import javax.sql.DataSource;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class App {

    private DataSource dataSource;
    private TimestampDao timestampDao;
    private Runnable timeStampProducer;
    private Runnable timeStampConsumer;


    public App(){
        try {
            AppConfig.init();

            dataSource = DataSourceFactory.getDataSource();
            CacheService<Date> cacheService = new CacheServiceInMemoryQueueImpl<>(new LinkedBlockingQueue<>());
            timestampDao = new TimestampDaoMySqlImpl(dataSource);
            timeStampProducer = new TimeStampProducer(cacheService);
            timeStampConsumer = new TimeStampConsumer(cacheService, timestampDao);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public App(DataSource dataSource, Runnable timeStampProducer, Runnable timeStampConsumer){
        this.dataSource = dataSource;
        this.timeStampProducer = timeStampProducer;
        this.timeStampConsumer = timeStampConsumer;
    }

    public static void main(String[] args) {
        new App().start(args);
    }


    public void start(String[] args){
        try {
            if (args.length > 0 && args[0].equals("-p")){
                List<Date> read = timestampDao.read();

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                read.forEach(date -> System.out.println(dateFormat.format(date)));
            } else {
                Thread producer = new Thread(timeStampProducer);
                producer.start();
                Thread consumer = new Thread(timeStampConsumer);
                consumer.start();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
