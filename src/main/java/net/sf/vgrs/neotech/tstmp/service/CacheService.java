package net.sf.vgrs.neotech.tstmp.service;

public interface CacheService<T> {

    public void put(T element) throws InterruptedException;

    public T get() throws InterruptedException;
}
