package net.sf.vgrs.neotech.tstmp.dao;

import net.sf.vgrs.neotech.tstmp.domain.exceptions.DatabaseInterruption;
import net.sf.vgrs.neotech.tstmp.domain.exceptions.UserDefinedException;

import java.util.Date;
import java.util.List;

public interface TimestampDao {

    public void create(Date date)throws UserDefinedException ;

    public List<Date> read() throws UserDefinedException;

}
