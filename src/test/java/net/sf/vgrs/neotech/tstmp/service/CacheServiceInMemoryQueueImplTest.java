package net.sf.vgrs.neotech.tstmp.service;

import org.junit.Before;
import org.junit.Test;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.Assert.*;

public class CacheServiceInMemoryQueueImplTest {

    private CacheService<Date> cacheService;

    @Before
    public void init(){
        cacheService = new CacheServiceInMemoryQueueImpl<>(new LinkedBlockingQueue<>());
    }

    @Test
    public void get() throws InterruptedException {
        Date date1 = new Date();
        Thread.sleep(1);
        Date date2 = new Date();
        cacheService.put(date1);
        cacheService.put(date2);

        Date date3 = cacheService.get();
        Date date4 = cacheService.get();

        assertEquals(date1, date3);
        assertEquals(date2, date4);

        System.out.println(date3);
        System.out.println(date4);

        assertTrue(date4.after(date3));
    }
}